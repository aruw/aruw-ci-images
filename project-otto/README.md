# Project Otto CI image

## Building and publishing the container image

_Make sure you have `jq` installed. On Ubuntu: `sudo apt install jq`._

Images are versioned by the date they were created, with an incrementing numeric suffix if there are
multiple builds on the same day. Images then have an additional suffix for which python version
they use. For example, `2020-08-25.3.cp38`.

First, make your Dockerfile changes and build the new images:
```bash
make build
```

The images are now locally tagged as `aruw/project-otto-ci:latest-cp38` and `aruw/project-otto-ci:latest-cp310`.
If it works like you expect, publish the new image:

```bash
make publish
```

You may have to authenticate using your gitlab account and a personal access token (you can create one in your user settings)

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

`publish` will automatically pick up the current date from your local system clock and appropriate
suffix from Docker Hub.
