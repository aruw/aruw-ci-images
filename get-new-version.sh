#!/bin/sh

TAGS_API_URL="https://gitlab.com/api/v4/projects/36327565/registry/repositories/$1/tags"

ver_base=$(TZ='America/Los_Angeles' date +%F)

curl --silent "$TAGS_API_URL" \
    | jq -r "
        [
            .[]
            | .name
            | select(startswith(\"$ver_base\"))
	    | scan(\"(?<=\\\\d{4}\\\\-\\\\d{2}\\\\-\\\\d{2}\\\\.)(?<ver>\\\\d+)(?=\\\\.?)\")[0]
	    | tonumber
        ] | max // 0
        | .+1
        | tostring
        | \"$ver_base.\" + .
    "
